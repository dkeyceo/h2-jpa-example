package com.dkey.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dkey.repository.PostRepository;

@RestController
@RequestMapping("/rest/posts")
public class PostController {
	
	@Autowired
	private PostRepository postRepository;
	
	@GetMapping("/all")
	public ResponseEntity getAllPosts() {
		return ResponseEntity.ok(postRepository.findAll());
	}

}
