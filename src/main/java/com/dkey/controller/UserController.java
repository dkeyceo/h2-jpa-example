package com.dkey.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dkey.repository.UserRepository;

@RestController
@RequestMapping("/rest/users")
public class UserController {
	@Autowired
	private UserRepository userRepository;
	
	@GetMapping("/all")
	private ResponseEntity getAllUsers() {
		return ResponseEntity.ok(userRepository.findAll());
	}
}
