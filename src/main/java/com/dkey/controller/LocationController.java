package com.dkey.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dkey.repository.LocationRepository;

@RestController
@RequestMapping("/rest/locations")
public class LocationController {
	@Autowired
	private LocationRepository locationRepository;
	@GetMapping("/all")
	public ResponseEntity getAllLocations() {
		return ResponseEntity.ok(locationRepository.findAll());
	}

}
