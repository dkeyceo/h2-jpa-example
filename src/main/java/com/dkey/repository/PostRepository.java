package com.dkey.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dkey.model.Post;
@Repository
public interface PostRepository extends JpaRepository<Post, Integer> {

}
