package com.dkey.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dkey.model.User;
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

}
